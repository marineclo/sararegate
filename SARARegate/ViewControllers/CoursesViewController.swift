//
//  CoursesViewController.swift
//  SARARegate
//
//  Created by Marine on 13.04.21.
//

import UIKit
import SARAFramework

class CoursesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SelectButtonCellDelegate, FirebaseManagerDelegate {

    // MARK: - UIOutlets
    @IBOutlet weak var newCourseButton: UIButton!
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Variables
    var courses: [Course]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // To update view when the route changed
        FirebaseManager.shared.delegate = self
        
        self.title = NSLocalizedString(Utils.localizedString(forKey: "menu_courses", app: "SARARegate"), comment: "")
        
        self.newCourseButton.customizePrimary()
        self.newCourseButton.setTitle(Utils.localizedString(forKey: "create_course", app: "SARARegate"), for: .normal)
        self.newCourseButton.accessibilityLabel = NSLocalizedString(Utils.localizedString(forKey: "create_course", app: "SARARegate"), comment: "")

        self.tableView.customize()
        let bundleView = Bundle(for: CoursesViewController.self)
        self.tableView.register(UINib(nibName: "SelectButtonCell", bundle: bundleView), forCellReuseIdentifier: "chooseCourseCellIdentifier")
        self.tableView.register(UINib(nibName: "HeaderTableView", bundle: bundleView), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // The courses are sorted by proximity
        self.courses = Course.allSortedByProximityFirstPoint()
        self.tableView?.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed), name: NSNotification.Name(rawValue:"modalCreateCourseIsDimissed"), object: nil) // From Create Course
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleModalDismissed() {
      self.courses = Course.allSortedByProximityFirstPoint()
      self.tableView?.reloadData()
    }
    
    // MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.courses?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "chooseCourseCellIdentifier") as? SelectButtonCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.setTitle(title: self.courses?[indexPath.row].name)
        cell.setId(id: self.courses?[indexPath.row].id)
        let bundle = Utils.bundle(anyClass: self.classForCoder)
        if let course = self.courses?[indexPath.row] {
            switch course.status {
            case .shared:
                let image = UIImage(named: "share", in: bundle, compatibleWith: nil)
                cell.setInverseImageView(image: image, accessibilityLabel: NSLocalizedString("shared", bundle: bundle, comment: ""))
            case .subscribed:
                let image = UIImage(named: "subscribe", in: bundle, compatibleWith: nil)
                cell.setInverseImageView(image: image, accessibilityLabel: NSLocalizedString("subscribed", bundle: bundle, comment: ""))
            default:
                cell.setInverseImageView()
            }
        } else {
            cell.setInverseImageView()
        }
        if self.courses?[indexPath.row] == NavigationManager.shared.route.value { // It's the enabled course
            cell.selectButton?.setImage(UIImage(named: "globe", in: bundle, compatibleWith: nil), for: .normal)
            cell.selectButton?.isEnabled = true
            cell.selectButton?.tintColor = Utils.tintIcon
            cell.selectButton?.accessibilityLabel = "\(Utils.localizedString(forKey: "activated_course", app: "SARARegate")) \(NavigationManager.shared.route.value!.name)"
        } else {
            cell.selectButton?.setImage(UIImage(named: "circle", in: bundle, compatibleWith: nil), for: .normal)
            let course = self.courses?[indexPath.row]
            if course?.points.count == 0 { // The course has no points
                cell.selectButton?.tintColor = UIColor.lightGray
                cell.selectButton?.isEnabled = false
                cell.selectButton?.accessibilityLabel = "\(Utils.localizedString(forKey: "course_no_points_not_activable", app: "SARARegate"))"
            } else if let _course = course, !_course.allPointsHasCoordinates() { // marks has no coordinates
                cell.selectButton?.tintColor = UIColor.lightGray
                cell.selectButton?.isEnabled = false
                cell.selectButton?.accessibilityLabel = "\(Utils.localizedString(forKey: "course_points_no_coord", app: "SARARegate"))"
            } else {
                cell.selectButton?.isEnabled = true
                cell.selectButton?.accessibilityLabel = "\( Utils.localizedString(forKey: "activate_course", app: "SARARegate")) \(self.courses?[indexPath.row].name ?? "")"
                cell.selectButton?.tintColor = Utils.tintIcon
            }
            cell.accessoryType = .none
        }
        cell.accessibilityTraits = .button
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: CoursesViewController.classForCoder()))
        guard let controller = storyboard.instantiateViewController(withIdentifier: "CreateCourseViewController") as? CreateCourseViewController,
            let course = self.courses?[indexPath.row] else {
            return
        }
        if let raceCourse = course as? RaceCourse {
            controller.viewModel = CreateRaceCourseViewModel(course: raceCourse)
        }
        self.present(controller, animated: true, completion: nil)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: Utils.localizedString(forKey: "courses_list", app: "SARARegate"))
        return returnedView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    //MARK: - SelectButtonCellDelegate
    public func didSelectCell(id: String) { // To active a course
        guard let course = self.courses?.first(where: {$0.id == id}) else {
            return
        }
        if NavigationManager.shared.route.value?.id == id { // The course is the current one, disactivate it
            NavigationManager.shared.setCurrentRoute(route: nil)
            if let index = self.courses?.firstIndex(of: course) { // Get the index of the course to update view
                self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
            }
        } else {
            NavigationManager.shared.updateNavView = true
            NavigationManager.shared.setCurrentRoute(route: course)
        }
    }
    
    // MARK: - FirebaseManagerDelegate
    func courseChanged(course: Course?) {
        self.courses = Course.allSortedByProximityFirstPoint()
        self.tableView?.reloadData()
    }
    
    func courseRemoved(courseId: String) {
        self.courses = Course.allSortedByProximityFirstPoint()
        self.tableView?.reloadData()
    }
    
    func courseAdded(course: Course?) {}
}
