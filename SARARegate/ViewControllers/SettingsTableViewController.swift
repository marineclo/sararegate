//
//  SettingsTableViewController.swift
//  SARARegate
//
//  Created by Marine on 31.05.21.
//  Copyright © 2021 GraniteApps. All rights reserved.
//

import UIKit
import SARAFramework

class SettingsTableViewController: UITableViewController {

    let bundle = Utils.bundle(anyClass: SettingsTableViewController.classForCoder())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("menu_settings", bundle: bundle, comment: "")
        
        self.tableView.register(UINib(nibName: "ImageCell", bundle: Bundle(for: SettingsTableViewController.self)), forCellReuseIdentifier: "menuCellIdentifier")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let imageCell = tableView.dequeueReusableCell(withIdentifier: "menuCellIdentifier") as? ImageCell else {
            return UITableViewCell()
        }
        
        imageCell.accessibilityTraits = UIAccessibilityTraits.button
        imageCell.accessoryType = .disclosureIndicator
        
        switch indexPath.section {
        case 0:
            imageCell.setTitle(title: NSLocalizedString("menu_general", bundle: bundle, comment: ""))
            imageCell.setImage(image: UIImage(named: "helm", in: bundle, compatibleWith: nil))
        case 1:
            imageCell.setTitle(title: NSLocalizedString("menu_traces", bundle: bundle, comment: ""))
            imageCell.setImage(image: UIImage(named: "binoculars", in: bundle, compatibleWith: nil))
        case 2:
            imageCell.setTitle(title: NSLocalizedString("menu_about", bundle: bundle, comment: ""))
            imageCell.setImage(image: UIImage(named: "anchor", in: bundle, compatibleWith: nil))
        default:
            break
        }
        return imageCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            performSegue(withIdentifier: "generalSegue", sender: nil)
        case 1:
            performSegue(withIdentifier: "tracesListSegue", sender: nil)
        case 2:
            performSegue(withIdentifier: "aboutSegue", sender: nil)
        default:
            break
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "generalSegue" {
            guard let destination = segue.destination as? GeneralViewController else {
                return
            }
            // For SARA Race, we don't need the validation point info
            destination.setDisplayValidationPoint(value: false)
        }
    }
}
