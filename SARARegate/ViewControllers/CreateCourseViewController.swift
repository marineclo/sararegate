//
//  CreateCourseViewController.swift
//  SARARegate
//
//  Created by Marine on 16.04.21.
//

import UIKit
import SARAFramework
import RxSwift
import Reachability

class CreateCourseViewController: UIViewController, UITextFieldDelegate /*, UITableViewDataSource */, UITableViewDelegate, FirebaseManagerDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var okButton: UIBarButtonItem!
    @IBOutlet weak var titleItem: UINavigationItem!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var typeButton: UIButton!
    @IBOutlet weak var lapsView: CustomSliderView!
    @IBOutlet weak var autoCreateButton: UIButton!
    @IBOutlet weak var addMarkButton: UIButton!
    @IBOutlet weak var reorganiseButton: UIButton!
    @IBOutlet weak var marksTableView: UITableView!
    @IBOutlet weak var visualizeButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    //MARK: - Variables
    let bundle = Utils.bundle(anyClass: CreateCourseViewController.classForCoder())
    var viewModel: CreateRaceCourseViewModel?
    let disposeBag = DisposeBag()
    var isInitView: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // To update view when the route changed
        FirebaseManager.shared.delegate = self
        
        if self.viewModel == nil {
            self.viewModel = CreateRaceCourseViewModel(course: nil)
        }
        
        if self.viewModel?.getCourseName() != nil {
            self.titleItem.title = Utils.localizedString(forKey: "edit_course", app: "SARARegate")
        } else {
            self.titleItem.title = Utils.localizedString(forKey: "create_course", app: "SARARegate")
        }
        
        self.cancelButton.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
        self.okButton.title = NSLocalizedString("ok", bundle: bundle, comment: "")
        // Labels
        self.nameLabel.text = NSLocalizedString("name", bundle: bundle, comment: "")
        self.nameLabel.isAccessibilityElement = false
        self.typeLabel.text = NSLocalizedString("type", bundle: bundle, comment: "")
        self.typeLabel.isAccessibilityElement = false
        // TextField
        self.nameTextField.text = self.viewModel?.getCourseName()
        self.nameTextField.accessibilityLabel = NSLocalizedString("name", bundle: bundle, comment: "")
        self.nameTextField
            .rx.text
            .map { $0?.isNotEmpty ?? false }
            .bind(onNext: { (isName) in
                self.okButton.isEnabled = isName
                self.activateButton.isEnabled = isName && ((self.viewModel?.numberOfPoints() ?? 0) >= 1)
                self.shareButton.isEnabled = isName
            })
            .disposed(by: disposeBag)
        // Subscribed course: disable edition
        self.nameTextField.isEnabled = self.enabledEditing() ? false : true
        
        // Buttons
        isInitView = true
        setupButtons()
        // LapsView
        self.lapsView.customize(with: SliderInfos(title: Utils.localizedString(forKey: "laps_number", app: "SARARegate"), unit: nil, value: Float(self.viewModel?.course.laps.value ?? 1), minValue: 1, maxValue: 7, isContinuous: false))
        self.lapsView.isEnabled = self.viewModel?.type.value != nil && self.viewModel?.type.value != .trainingStart && self.viewModel?.type.value != .trainingFinish && !enabledEditing()  // Disable button if no type, or training start, trainish finish or subscribed course
        self.lapsView.settingsValue.asObservable()
            .bind(onNext: { newVal in
                if Int(newVal) != self.viewModel?.laps.value { // New value: need to update the race
                    if Int(newVal) > self.viewModel?.laps.value ?? 0 {
                        self.viewModel?.addMarksLaps()
                    }
                    if Int(newVal) < self.viewModel?.laps.value ?? 0 {
                        self.viewModel?.removeMarksLaps()
                    }
                    self.viewModel?.laps.accept(Int(newVal))
                }
            }).disposed(by: disposeBag)
        
        // TableView
        bindTableView()
        self.marksTableView.register(UINib(nibName: "HeaderTableView", bundle: Bundle(for: CreateCourseViewController.classForCoder())), forHeaderFooterViewReuseIdentifier: "HeaderTableView")
        self.marksTableView.customize()
        
        // Close keyboard when tap on the view
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.elementsToLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed(note:)), name: NSNotification.Name(rawValue:"modalCreatePointIsDimissed"), object: nil) // From AddPointSegmented
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed(note:)), name: NSNotification.Name(rawValue:"modalCreateMarkIsDimissed"), object: nil) // From Mark edition
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed(note:)), name: NSNotification.Name("modalChoosePointIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleModalDismissed(note:)), name: NSNotification.Name("modalAutoCreationIsDimissed"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleModalDismissed(note: Notification) {
        self.elementsToLoad()
        let isOk = !viewModel!.getTempPoints().map({$0.getCoordinates(method: .middle) == nil}).contains(true) && self.viewModel!.getTempPoints().count > 0
        // To activate course: at least one point, all point have coordinates and the course has a name
        self.activateButton.isEnabled = isOk && self.nameTextField.text?.isNotEmpty ?? false
        // To visualize course: at least one point, all point have coordinates
        self.visualizeButton.isEnabled = isOk
        switch note.name {
        case NSNotification.Name(rawValue: "modalAutoCreationIsDimissed"):
            if self.viewModel?.buoyRef.value != nil { // if we use auto creation, not possible to add mark or reorganise
                self.addMarkButton.isEnabled = false
                self.reorganiseButton.isEnabled = false
            }
            break
        case NSNotification.Name(rawValue: "modalCreateMarkIsDimissed"), NSNotification.Name(rawValue: "modalCreatePointIsDimissed"), NSNotification.Name(rawValue: "modalChoosePointIsDimissed"):
            self.typeButton.isEnabled = self.viewModel!.getTempPoints().isEmpty ? true : false
            self.marksTableView.reloadData()
            break
        default:
            return
        }
    }
        
    func elementsToLoad() {
        if self.viewModel?.course == NavigationManager.shared.route.value {
            self.viewModel?.currentPointIndex = NavigationManager.shared.currentPointIndex.value
        }
    }
    
    func bindTableView() {
        viewModel?.temporaryPointsB.bind(to: marksTableView.rx.items(cellIdentifier: "chooseCourseCellIdentifier", cellType: UITableViewCell.self)) { (row, item, cell) in
            guard let mark = item as? Mark else { return }
            cell.textLabel?.text = mark.name
            cell.tintColor = Utils.tintIcon
            cell.accessibilityLabel = "\(mark.name)" //TODO name + type or name + no coordinates
//            mark.getCoordinates(method: .middle)
        }.disposed(by: disposeBag)
        marksTableView.rx
            .itemSelected
            .subscribe(onNext: { indexPath in
                let storyboard = UIStoryboard(name: "Route", bundle: Bundle(for: CreateCourseViewController.classForCoder()))
                //Instantiate View Controller
                if let mark = self.viewModel?.getPointAtIndexPath(indexPath) as? Mark {
                    guard let controller = storyboard.instantiateViewController(withIdentifier: "CreateMarkViewController") as? CreateMarkViewController else {
                        return
                    }
                    controller.setMark(mark)
                    controller.viewModel = self.viewModel
                    controller.indexInRoute = indexPath.row
                    self.present(controller, animated: true, completion: nil)
                    self.marksTableView.deselectRow(at: indexPath, animated: false)
                }
            }).disposed(by: disposeBag)
        marksTableView.rx
            .itemDeleted
            .subscribe(onNext: { indexPath in
                // Desactivate route when the point is used in the current route
                if let mark = self.viewModel?.getPointAtIndexPath(indexPath) as? Mark, let course = NavigationManager.shared.route.value, self.viewModel?.course == course, course.points.contains(where: {$0.primaryKey == mark.id})  {
                    let message = Utils.localizedString(forKey: "warning_delete_used_mark", app: "SARARegate")
                    let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: self.bundle, comment: ""), message: message, preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: self.bundle, comment: ""), style: .default) { _ in
                        self.viewModel?.currentPointIndex = NavigationManager.shared.currentPointIndex.value
                        if self.viewModel?.currentPointIndex != nil && (self.viewModel?.currentPointIndex)! > 0 {
                            self.viewModel?.currentPointIndex! -= 1
                        }
                        self.viewModel?.removeFromRoute(at: indexPath)
                        self.typeButton.isEnabled = self.viewModel != nil && self.viewModel?.type.value == nil && self.viewModel!.getTempPoints().isEmpty ? true : false
                    }
                    let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: self.bundle, comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(noAction)
                    alertController.addAction(yesAction)
                    self.present(alertController, animated: true, completion: nil)
                } else {
                    self.viewModel?.removeFromRoute(at: indexPath)
                    self.typeButton.isEnabled = self.viewModel != nil && self.viewModel?.type.value == nil && self.viewModel!.getTempPoints().isEmpty ? true : false
                }
            }).disposed(by: disposeBag)
        
        marksTableView.rx
            .itemMoved
            .subscribe(onNext: { itemMovedEvent in
                // Retrieve point to move
                let sourceIndexPath = itemMovedEvent.sourceIndex
                let destinationIndexPath = itemMovedEvent.destinationIndex
                guard let pointToMove = self.viewModel?.getPointAtIndexPath(sourceIndexPath) else {
                    return
                }
                //Remove it from old position and insert it to new position
                self.viewModel?.removeFromRoute(at: sourceIndexPath)
                self.viewModel?.addToRoute(pointToMove, at: destinationIndexPath)
            }).disposed(by: disposeBag)
 
        viewModel?.temporaryPointsB // at least one point, all point have coordinates
            .map({!$0.map({ $0.getCoordinates(method: .middle) == nil}).contains(true) && $0.count > 0})
            .bind(onNext: { isOk in
                // To activate course: at least one point, all point have coordinates and the course has a name, not table view in edit mode (reorganise)
                self.activateButton.isEnabled = isOk && self.nameTextField.text?.isNotEmpty ?? false && !self.marksTableView.isEditing
                // To visualize course: at least one point, all point have coordinates, not table view in edit mode (reorganise)
                self.visualizeButton.isEnabled = isOk && !self.marksTableView.isEditing
            }).disposed(by: disposeBag)
    }
    
    // Enable modify course: if the course is subscribed or the course is the one activated, disable modification
    func enabledEditing() -> Bool {
        return self.viewModel?.status == .subscribed || NavigationManager.shared.route.value?.id == self.viewModel?.course.id
    }

    func setupButtons() {
        self.typeButton.customizePrimary()
        let typeLoc = self.viewModel?.course.type.value?.description ?? Utils.localizedString(forKey: "choose_type", app: "SARARegate")
        self.typeButton.setTitle(typeLoc, for: .normal)
        self.typeButton.accessibilityLabel = typeLoc
        self.typeButton.isEnabled = self.viewModel != nil && self.viewModel!.course.id.isEmpty // Disable button in edit mode
        self.viewModel?.type.asObservable()
            .bind(onNext: { type in
                switch type {
                case .matchRace: // two laps by default for match race
                    if !self.isInitView {
                        self.lapsView.settingsValue.accept(2)
                    }
                    self.lapsView.isEnabled = self.enabledEditing() ? false : true
                    self.autoCreateButton.isEnabled = self.enabledEditing() ? false : true
                    self.addMarkButton.isEnabled = false
                    self.reorganiseButton.isEnabled = false
                case .bananeLeewardGate, .bananeDoglegLeewardGate:
                    self.autoCreateButton.isEnabled = false
                    if !self.isInitView {
                        self.lapsView.settingsValue.accept(Float(self.viewModel?.laps.value ?? 1))
                    }
                    self.lapsView.isEnabled = self.enabledEditing() ? false : true
                    self.addMarkButton.isEnabled = false
                    self.reorganiseButton.isEnabled = false
                case .trainingStart, .trainingFinish:
                    self.autoCreateButton.isEnabled = false
                    if !self.isInitView {
                        self.lapsView.settingsValue.accept(Float(self.viewModel?.laps.value ?? 1))
                    }
                    self.lapsView.isEnabled = false
                    self.addMarkButton.isEnabled = false
                    self.reorganiseButton.isEnabled = false
                case .trainingLoop:
                    self.autoCreateButton.isEnabled = false
                    if !self.isInitView {
                        self.lapsView.settingsValue.accept(Float(self.viewModel?.laps.value ?? 1))
                    }
                    self.lapsView.isEnabled = self.enabledEditing() ? false : true
                    self.addMarkButton.isEnabled = false
                    self.reorganiseButton.isEnabled = false
                default:
                    if !self.isInitView {
                        self.lapsView.settingsValue.accept(Float(self.viewModel?.laps.value ?? 1))
                    }
                    self.autoCreateButton.isEnabled = self.enabledEditing() ? false : true
                    self.lapsView.isEnabled = self.enabledEditing() ? false : true
                    self.addMarkButton.isEnabled = false
                    self.reorganiseButton.isEnabled = false
                    break
                }
                self.isInitView = false
            }).disposed(by: disposeBag)
        
        self.autoCreateButton.customizePrimary()
        self.autoCreateButton.isEnabled = self.viewModel?.type.value != nil && self.viewModel?.type.value != .trainingStart && self.viewModel?.type.value != .trainingFinish && self.viewModel?.type.value != .trainingLoop && self.viewModel?.type.value != .bananeLeewardGate && self.viewModel?.type.value != .bananeDoglegLeewardGate && !self.enabledEditing() // Disable button if no type, training start, training finish or training loop or subscrbed course or enabled course
        self.autoCreateButton.setTitle(Utils.localizedString(forKey: "auto_creation_abb", app: "SARARegate"), for: .normal)
        self.autoCreateButton.accessibilityLabel = Utils.localizedString(forKey: "auto_creation", app: "SARARegate")
        
        self.addMarkButton.customizePrimary()
        self.addMarkButton.setTitle(NSLocalizedString("add_mark", bundle: bundle, comment: ""), for: .normal)
        self.addMarkButton.accessibilityLabel = NSLocalizedString("add_mark", bundle: bundle, comment: "")
        self.addMarkButton.isEnabled = self.viewModel?.type.value == nil // Disable button if "route type"
        
        self.reorganiseButton.customizeSecondary()
        self.reorganiseButton.setTitle(NSLocalizedString("reorganize", bundle: bundle, comment: ""), for: .normal)
        self.reorganiseButton.accessibilityLabel = NSLocalizedString("reorganize", bundle: bundle, comment: "")
        self.reorganiseButton.isEnabled = self.viewModel?.type.value == nil // Disable button if "route type"
        
        self.visualizeButton.customizeSecondary()
        self.visualizeButton.setTitle(NSLocalizedString("visualize", bundle: bundle, comment: ""), for: .normal)
        self.visualizeButton.accessibilityLabel = NSLocalizedString("visualize", bundle: bundle, comment: "")

        self.shareButton.customizeSecondary()
        let title: String = (self.viewModel != nil && self.viewModel!.status == .shared) ? "unshare" : "share"
        self.shareButton.setTitle(NSLocalizedString(title, bundle: bundle, comment: ""), for: .normal)
        self.shareButton.accessibilityLabel = NSLocalizedString(title, bundle: bundle, comment: "")
        self.shareButton.isEnabled = User.shared.email.value != nil && self.viewModel?.status != .subscribed && self.nameTextField.text?.isNotEmpty ?? false
        
        self.activateButton.customizeSecondary()
        if let course = self.viewModel?.course, course == NavigationManager.shared.route.value {
            self.activateButton.setTitle(NSLocalizedString("deactivate", bundle: bundle, comment: ""), for: .normal)
            self.activateButton.accessibilityLabel = NSLocalizedString("deactivate", bundle: bundle, comment: "")
        } else {
            self.activateButton.setTitle(NSLocalizedString("activate", bundle: bundle, comment: ""), for: .normal)
            self.activateButton.accessibilityLabel = NSLocalizedString("activate", bundle: bundle, comment: "")
        }
        
        self.deleteButton.customizeSecondary()
        self.deleteButton.setTitle(NSLocalizedString("delete", bundle: bundle, comment: ""), for: .normal)
        self.deleteButton.accessibilityLabel = NSLocalizedString("delete", bundle: bundle, comment: "")
        if self.viewModel?.course.id == "" || self.viewModel?.status == .subscribed {
            self.deleteButton.isEnabled = false
        } else {
            NavigationManager.shared.route.asObservable()
                .bind(onNext: { course in
                    if course != self.viewModel?.course { // Not possible to delete the course if activates
                        self.deleteButton.isEnabled = true
                        self.deleteButton.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: .normal)
                    } else {
                        self.deleteButton.isEnabled = false
                    }
                }).disposed(by: disposeBag)
        }
    }
    
    private func stopEditing() {
        // Check that there is not same point consecutively when clicking on validate
        if self.marksTableView.isEditing && self.viewModel != nil && self.viewModel!.consecutivelyPoints() {
            let alert = Utils.showAlert(with: NSLocalizedString("warning", bundle: self.bundle, comment: ""), message: NSLocalizedString("warning_same_point_consecutively", bundle: self.bundle, comment: ""))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.marksTableView.isEditing = !self.marksTableView.isEditing
            // Disable buttons when editing mode and re-enable it after
            self.addMarkButton.isEnabled = !self.marksTableView.isEditing
            let isOk = !self.viewModel!.getTempPoints().map({$0.getCoordinates(method: .middle) == nil}).contains(true) && self.viewModel!.getTempPoints().count > 0
            self.visualizeButton.isEnabled = !self.marksTableView.isEditing && isOk
            self.activateButton.isEnabled = !self.marksTableView.isEditing && isOk && self.nameTextField.text?.isNotEmpty ?? false
//            self.shareButton.isEnabled = !self.marksTableView.isEditing
            self.deleteButton.isEnabled = !self.marksTableView.isEditing && self.viewModel?.course.id != ""
            let title = self.marksTableView.isEditing ? NSLocalizedString("reorganize_points_validate", bundle: bundle, comment: "") : NSLocalizedString("reorganize", bundle: bundle, comment: "")
            self.reorganiseButton.setTitle(title, for: .normal)
            self.reorganiseButton.accessibilityLabel = title
        }
    }
    
    // Display an alert if there are errors when saving
    func errorAlertSave(errorMessages: [String]) {
        let message = errorMessages.count > 1 ? errorMessages.map({ "• \($0)" }).joined(separator: "\n") : errorMessages.joined(separator: "\n")
        let alert = UIAlertController(title: Utils.localizedString(forKey: "warning_save_route", app: "SARARegate"), message: message, preferredStyle: .alert)
        if errorMessages.count > 1 { // Bullet list
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = NSTextAlignment.left
            let attributedMessage: NSMutableAttributedString = NSMutableAttributedString(
                string: message, // your string message here
                attributes: [
                    NSAttributedString.Key.paragraphStyle: paragraphStyle,
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)
                ]
            )
            alert.setValue(attributedMessage, forKey: "attributedMessage")
        }
        let yesAction = UIAlertAction(title: NSLocalizedString("ok", bundle: self.bundle, comment: ""), style: .cancel)
        alert.addAction(yesAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = self.marksTableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderTableView" ) as! HeaderTableView
        returnedView.customizeHeader(titleName: Utils.localizedString(forKey: "marks_list", app: "SARARegate"))
        return returnedView
    }

    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    public func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if self.viewModel?.type.value != nil {
            return .none
        } else {
            return .delete
        }
    }
    
    // MARK: - IBActions
    @IBAction func cancelAction(_ sender: Any) {
        present(Utils.showCancelAlert(controllerToDismiss: self), animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCourseIsDimissed"), object: nil)
        })
    }
    
    @IBAction func okAction(_ sender: Any) {
        guard let name = self.nameTextField.text, let _viewModel = self.viewModel else {
            return
        }
        if _viewModel.status == .subscribed { // Don't save the course if it's a subscribed course
            self.dismiss(animated: true, completion: {
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCourseIsDimissed"), object: nil)
            })
            return
        }
        // Save course or add messages if errors
        _viewModel.saveRoute(name: name, update: (_viewModel.course.name != ""), successHandler: {
            if self.viewModel?.course == NavigationManager.shared.route.value {
                // set the route
                NavigationManager.shared.setCurrentRoute(route: self.viewModel?.course)
                if let currentPointIndex = self.viewModel?.currentPointIndex {
                    NavigationManager.shared.setPoint(with: currentPointIndex)
                }
            }
            self.dismiss(animated: true, completion: {
                  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCourseIsDimissed"), object: nil)
            })
        }, errorHandler: {errorMessages in
            self.errorAlertSave(errorMessages: errorMessages)
        })
    }
    
    @IBAction func selectTypeAction(_ sender: Any) {
        let typesMenu = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        for raceType in RaceCourseType.allCases {
            let typeAction = UIAlertAction(title: raceType.description, style: .default, handler: { [weak self] _ in
                let marks = raceType.marksForType
                self?.viewModel?.setTempPoints(points: [])
                marks.forEach({
                    self?.viewModel?.addToRoute($0)
                })
                self?.typeButton.setTitle(raceType.description, for: .normal)
                self?.typeButton.accessibilityLabel = raceType.description
                self?.viewModel?.setType(type: raceType)
            })
            typesMenu.addAction(typeAction)
        }
        // Adding cancel action to go back
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: self.bundle, comment: ""), style: .cancel, handler: nil)
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        typesMenu.addAction(cancelAction)
        
        // Present action sheet
        present(typesMenu, animated: true, completion: nil)
    }
    
    @IBAction func reorganiseAction(_ sender: Any) {
        stopEditing()
    }
    
    @IBAction func shareAction(_ sender: Any) {
        guard let viewModel = self.viewModel, let name = self.nameTextField.text else {
            return
        }
        if FirebaseManager.shared.reachability.connection == .unavailable {
            let alert = Utils.showAlert(with: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("no_internet_connection_no_sharing", bundle: bundle, comment: ""))
            present(alert, animated: false)
        } else if viewModel.status == .shared { // Unshare course
            FirebaseManager.shared.unshareCourse(course: viewModel.course, successHandler: {
                viewModel.status = .none
                let title = NSLocalizedString("share", bundle: self.bundle, comment: "")
                self.shareButton.setTitle(title, for: .normal)
                self.shareButton.accessibilityLabel = title
                let alert = Utils.showAlert(with: "", message: NSLocalizedString("unshare_course_success", bundle: self.bundle, comment: ""))
                self.present(alert, animated: true, completion: nil)
            }, errorHandler: { error in
                self.errorAlertSave(errorMessages: [error.localizedDescription])
            })
        } else { // Save course
            viewModel.status = .shared
            viewModel.saveRoute(name: name, update: (viewModel.course.name != ""), successHandler: {
                let title = NSLocalizedString("unshare", bundle: self.bundle, comment: "")
                self.shareButton.setTitle(title, for: .normal)
                self.shareButton.accessibilityLabel = title
                let alert = Utils.showAlert(with: "", message: NSLocalizedString("share_course_success", bundle: self.bundle, comment: ""))
                self.present(alert, animated: true, completion: nil)
            }, errorHandler: {errorMessages in
                self.errorAlertSave(errorMessages: errorMessages)
                viewModel.status = self.viewModel!.course.status
            })
        }
    }
    
    
    @IBAction func activateAction(_ sender: UIButton) {
        guard let viewModel = self.viewModel else {
            return
        }
        if viewModel.course == NavigationManager.shared.route.value { // Click on the disable button
            NavigationManager.shared.setCurrentRoute(route: nil) // Disable route
            let title = NSLocalizedString("activate", bundle: bundle, comment: "")
            self.activateButton.setTitle(title, for: .normal)
            self.activateButton.accessibilityLabel = title
            if viewModel.autoCreationParams?.id != "" { // Enable auto creation button if the params exist
                self.autoCreateButton.isEnabled = true
            }
            return
        }
        if !viewModel.allPointsHasCoordinates() {
            let alert = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("warning_route_mark_no_coords", bundle: bundle, comment: ""), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .cancel)
            alert.addAction(yesAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            guard let name = self.nameTextField.text else {
                return
            }
            if viewModel.status == .subscribed { // Don't save the course
                let title = NSLocalizedString("deactivate", bundle: self.bundle, comment: "")
                self.activateButton.setTitle(title, for: .normal)
                self.activateButton.accessibilityLabel = title
                NavigationManager.shared.updateNavView = true
                NavigationManager.shared.setCurrentRoute(route: viewModel.course)
                self.dismiss(animated: true, completion: nil)
            } else {
                // Save course or add messages if errors
                viewModel.saveRoute(name: name, update: (viewModel.course.name != ""), successHandler: {
                    let title = NSLocalizedString("deactivate", bundle: self.bundle, comment: "")
                    self.activateButton.setTitle(title, for: .normal)
                    self.activateButton.accessibilityLabel = title
                    NavigationManager.shared.updateNavView = true
                    NavigationManager.shared.setCurrentRoute(route: viewModel.course)
                    self.dismiss(animated: true, completion: nil)
                }, errorHandler: { errorMessages in
                    self.errorAlertSave(errorMessages: errorMessages)
                })
            }
        }
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        if let route = self.viewModel?.course {
            if route.status == .shared {
                if FirebaseManager.shared.reachability.connection == .unavailable {
                    let alert = Utils.showAlert(with: NSLocalizedString("warning", bundle: bundle, comment: ""), message: NSLocalizedString("no_internet_connection_no_deleting_share", bundle: bundle, comment: ""))
                    present(alert, animated: false)
                } else {
                    let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: Utils.localizedString(forKey: "warning_delete_course_share", app: "SARARegate"), preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                        FirebaseManager.shared.unshareCourse(course: route, successHandler: {
                            
                        }, errorHandler: { error in
                            self.errorAlertSave(errorMessages: [error.localizedDescription])
                        })
                        route.removeCourseWithPoints()
                        self.dismiss(animated: true, completion: {
                              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCourseIsDimissed"), object: nil)
                        })
                    }
                    let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(noAction)
                    alertController.addAction(yesAction)
                    present(alertController, animated: true, completion: nil)
                }
            } else {
                let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: Utils.localizedString(forKey: "warning_delete_course", app: "SARARegate"), preferredStyle: .alert)
                let yesAction = UIAlertAction(title: NSLocalizedString("yes", bundle: bundle, comment: ""), style: .default) { _ in
                    route.removeCourseWithPoints()
                    self.dismiss(animated: true, completion: {
                          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCourseIsDimissed"), object: nil)
                    })
                }
                let noAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil)
                alertController.addAction(noAction)
                alertController.addAction(yesAction)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createMarkSegue" {
            guard let destination = segue.destination as? AddPointSegmentedViewController else {
                return
            }
            let storyboard = UIStoryboard(name: "Route", bundle: Bundle(for: CreateCourseViewController.classForCoder()))
            //Instantiate View Controller
            let createMarkVC = storyboard.instantiateViewController(withIdentifier: "CreateMarkViewController") as! CreateMarkViewController
            createMarkVC.viewModel = self.viewModel
            destination.viewControllersList.append(createMarkVC)
            let existingPoint = storyboard.instantiateViewController(withIdentifier: "ChoosePointViewController") as! ChoosePointViewController
            existingPoint.viewModel = self.viewModel
            existingPoint.isMarks = true
            destination.viewControllersList.append(existingPoint)
        } else if segue.identifier == "visualizeCourseTable" {
            guard self.viewModel?.numberOfPoints() ?? 0 >= 2 else {
                return
            }
            guard let destination = segue.destination as? CourseVisualizationViewController else {
                return
            }
            destination.viewModel = viewModel
            destination.titleNavigation =  nameTextField.text
        } else if segue.identifier == "autoCreationSegue" {
            guard let destination = segue.destination as? AutoCreationViewController else {
                return
            }
            destination.viewModel = viewModel
        }
    }
    
    // MARK: - FirebaseManagerDelegate
    func courseChanged(course: Course?) {
        //
        guard let raceCourse = course as? RaceCourse else { return }
        self.viewModel?.updateViewModel(course: raceCourse)
        
        self.nameTextField.text = self.viewModel?.getCourseName()
        self.lapsView.settingsValue.accept(Float(self.viewModel?.laps.value ?? 0))
    }

    func courseRemoved(courseId: String) {
    }
    
    func courseAdded(course: Course?) {
    }
}
