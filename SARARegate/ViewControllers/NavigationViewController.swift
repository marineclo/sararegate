//
//  NavigationViewController.swift
//  SARARegate
//
//  Created by Marine on 14.04.21.
//

import UIKit
import SARAFramework
import RxSwift
import RxCocoa

class NavigationViewController: UIViewController {

    // MARK: - UIOutlets
    @IBOutlet weak var courseLabel: ImageLabel!
    @IBOutlet weak var speedLabel: ImageLabel!
    @IBOutlet weak var compassLabel: ImageLabel!
    @IBOutlet weak var chooseCourseButton: UIButton!
    
    @IBOutlet weak var previousMarkButton: UIButton!
    @IBOutlet weak var playStopButton: UIButton!
    @IBOutlet weak var nextMarkButton: UIButton!
    
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var plotButton: UIButton!
    
    @IBOutlet weak var timerView: TimerView!
//    @IBOutlet weak var infoContainerView: UIView!
    @IBOutlet weak var tabsMenuContainerView: UIView!
    @IBOutlet var timerConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    let disposeBag = DisposeBag()
    let bundle = Utils.bundle(anyClass: NavigationViewController.classForCoder())
    
    private var navigationPagingController: NavigationPagingViewController? {
        didSet {
            navigationPagingController?.tabsMenuViewController = tabsMenuController
            tabsMenuController?.delegate = navigationPagingController
        }
    }
    private var tabsMenuController: TabsMenuViewController? {
        didSet {
            navigationPagingController?.tabsMenuViewController = tabsMenuController
            tabsMenuController?.delegate = navigationPagingController
        }
    }
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = Utils.primaryColor
        
        setupAndBindQuickInfo()
        setupBoutons()
        
        // Tabs menu
        self.tabsMenuController = TabsMenuViewController()
        self.addChild(self.tabsMenuController!)
        self.tabsMenuContainerView.addSubview(tabsMenuController!.view)
        self.tabsMenuController!.view.frame = CGRect(x: 0, y: 0, width: self.tabsMenuContainerView.frame.size.width, height: self.tabsMenuContainerView.frame.size.height)

        self.tabsMenuController!.menuBackgroundColor = Utils.primaryColor
        self.tabsMenuController!.menuItemsColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        NavigationManager.shared.isNotFirstPoint
            .asObservable()
            .bind(to: previousMarkButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        NavigationManager.shared.isNotLastPoint
            .asObservable()
            .bind(to: nextMarkButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        NavigationManager.shared.route
            .asObservable()
            .subscribe(onNext: { course in
                let name = NavigationManager.shared.route.value?.name ?? Utils.localizedString(forKey: "choose_course", app: "SARARegate")
                self.chooseCourseButton.setTitle(name, for: .normal)
                self.chooseCourseButton.moveImageLeftTextCenter()
                NavigationManager.shared.hasStarted.accept(false)
                if course == nil {
                    self.chooseCourseButton.accessibilityLabel = "\(Utils.localizedString(forKey: "choose_courses_list", app: "SARARegate")) \(Utils.localizedString(forKey: "no_activated_course", app: "SARARegate"))"
                    self.timerConstraint.isActive = true // remove timer view
                    self.timerView.isHidden = true
                    self.playStopButton.isEnabled = false // disable play button
                } else {
                    self.chooseCourseButton.accessibilityLabel = "\(Utils.localizedString(forKey: "choose_courses_list", app: "SARARegate")) \(Utils.localizedString(forKey: "course_title", app: "SARARegate")) \(course!.name) \(NSLocalizedString("activated", bundle: self.bundle, comment: ""))"
                    // Stop timer if there is one and Add timer only if there is a start for first mark
                    self.stopTimer()
                    self.playStopButton.isEnabled = true // enable play button
                }
                self.playButtonImageAccessibility()
            }).disposed(by: disposeBag)
                
        // If GPS accuracy >= 100m, disable plot button
//        LocationManager.shared.accuracyVariable
//            .asObservable()
//            .map({ return $0 != nil && $0! < 100 && NavigationManager.shared.timer == nil})
//            .bind(to: self.plotButton.rx.isEnabled)
//            .disposed(by: disposeBag)
        
        NavigationManager.shared.point
            .asObservable()
            .subscribe(onNext: { anypoint in
                if (NavigationManager.shared.hasStarted.value && !self.timerConstraint.isActive) && (anypoint != nil && !anypoint!.point.isStartPoint()){ // Remove timer if exits
                    self.stopTimer(isAddingTimerView: false)
                    self.timerConstraint.isActive = true // remove timer view
                    self.timerView.isHidden = true
                }
            }).disposed(by: disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if NavigationManager.shared.route.value != nil {
            chooseCourseButton.isEnabled = true
            chooseCourseButton.moveImageLeftTextCenter()
            muteButton.isEnabled = true
        } else {
            let enabledButton = Course.all().count != 0
            chooseCourseButton.isEnabled = enabledButton
            chooseCourseButton.alpha = enabledButton ? 1 : 0.5
        }
    }
    
    override func viewDidLayoutSubviews() {
        muteButton.layer.cornerRadius = muteButton.frame.size.width / 2
        muteButton.layer.borderWidth = 1
        muteButton.imageView?.contentMode = .scaleAspectFit
        muteButton.layer.backgroundColor = Utils.secondaryColor.cgColor
//        if AnnouncementQueueManager.shared.muted {
//            muteButton.tintColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
//            muteButton.layer.borderColor = Utils.secondaryColor.cgColor
//        } else {
            muteButton.tintColor = .white
//            muteButton.tintColor = Utils.secondaryColor
            muteButton.layer.borderColor = Utils.secondaryColor.cgColor
//        }
    }
    
    // MARK: - Functions
    
    func setupAndBindQuickInfo() {
        // Set types of the quick info top bar
        self.courseLabel.quickInfoType = .course
        self.speedLabel.quickInfoType = .speed
        self.compassLabel.quickInfoType = .compass
        
        // Bind quick info
        NavigationManager.shared.courseOverGround
            .asObservable()
            .map({
                return $0 != nil ? String(format: "%i", $0!) : NSLocalizedString("unavailable_nav_symbol", bundle: self.bundle, comment: "")
            })
            .bind(to: courseLabel.value)
            .disposed(by: disposeBag)
        
        NavigationManager.shared.speedOverGround
            .asObservable()
            .map({
                return $0 != nil ? String($0!) : NSLocalizedString("unavailable_nav_symbol", bundle: self.bundle, comment: "")
            })
            .bind(to: speedLabel.value)
            .disposed(by: disposeBag)
        
        LocationManager.shared.compassVariable
            .asObservable()
            .map({
                return $0 != nil ? String(format: "%i", Int($0!.trueHeading)) : NSLocalizedString("unavailable_nav_symbol", bundle: self.bundle, comment: "")
            })
            .bind(to: compassLabel.value)
            .disposed(by: disposeBag)
        
        // Reload speed unit if unit changes
        NavigationManager.shared.shouldReloadUnits
            .asObservable()
            .filter({ $0 == true })
            .subscribe(onNext: { [weak self] _ in
                self?.speedLabel.quickInfoUnit = Settings.shared.speedUnit?.unit
            }).disposed(by: disposeBag)
    }
    
    func setupBoutons() {
        // Choose course
        chooseCourseButton.customizeReverse()
        chooseCourseButton.setImage(UIImage(named: "route", in: self.bundle, compatibleWith: nil), for: .normal)
        chooseCourseButton.imageView?.tintColor = Utils.secondaryColor
        
        previousMarkButton.setTitleColor(Utils.disabledColor, for: .disabled)
        previousMarkButton.accessibilityLabel = Utils.localizedString(forKey: "activate_previous_mark", app: "SARARegate")
        previousMarkButton.tintColor = Utils.secondaryColor
        previousMarkButton.setImage(UIImage(named: "left-chevron", in: self.bundle, compatibleWith: nil), for: .normal)
        
        nextMarkButton.setTitleColor(Utils.disabledColor, for: .disabled)
        nextMarkButton.accessibilityLabel = Utils.localizedString(forKey: "activate_next_mark", app: "SARARegate")
        nextMarkButton.tintColor = Utils.secondaryColor
        nextMarkButton.setImage(UIImage(named: "right-chevron", in: self.bundle, compatibleWith: nil), for: .normal)
        
        playStopButton.setTitleColor(Utils.disabledColor, for: .disabled)
        playStopButton.tintColor = Utils.secondaryColor
        playButtonImageAccessibility()
        
        timerView.plusButton.rx.tap.subscribe(onNext: { _ in
            NavigationManager.shared.totalTime += NavigationManager.shared.incrementTimer
            if NavigationManager.shared.totalTime == 60 * 8 { // Value max: 8 minutes
                self.timerView.plusButton.isEnabled = false // disable button
            }
            if NavigationManager.shared.totalTime > 0 {
                self.timerView.minusButton.isEnabled = true // enable button
            }
            self.timerView.setTimer(value: NavigationManager.shared.totalTime)
        }).disposed(by: disposeBag)
        
        timerView.minusButton.rx.tap.subscribe(onNext: { _ in
            NavigationManager.shared.totalTime -= NavigationManager.shared.incrementTimer
            if NavigationManager.shared.totalTime == 0 { // Value min: 0 minutes
                self.timerView.minusButton.isEnabled = false // disable button
            }
            if NavigationManager.shared.totalTime < 60 * 8 {
                self.timerView.plusButton.isEnabled = true // enable button
            }
            self.timerView.setTimer(value: NavigationManager.shared.totalTime)
        }).disposed(by: disposeBag)
        
        muteButton.setImage(UIImage(systemName: "speaker.wave.3.fill"), for: .normal)
//        muteButton.setImage(UIImage(named: "volume-up", in: self.bundle, compatibleWith: nil), for: .normal)
//        muteButton.tintColor = Utils.secondaryColor
        muteButton.tintColor = .white
        muteButton.accessibilityLabel = NSLocalizedString("deactivate_announces", bundle: bundle, comment: "")
        muteButton.setTitle("", for: .normal)
        
        plotButton.setImage(UIImage(named: "course-point", in: self.bundle, compatibleWith: nil), for: .normal)
        plotButton.tintColor = Utils.secondaryColor
        plotButton.accessibilityLabel = Utils.localizedString(forKey: "plot_button", app: "SARARegate")
        // for first version disabled
//        plotButton.isEnabled = false
        plotButton.isHidden = true
    }
    
    func playButtonImageAccessibility() {
        let image = NavigationManager.shared.hasStarted.value ? (Settings.shared.getSaveTrace() ? UIImage(named: "stop-course-record", in: self.bundle, compatibleWith: nil) : UIImage(named: "stop-course", in: self.bundle, compatibleWith: nil)) : UIImage(named: "play-course", in: self.bundle, compatibleWith: nil)
        self.playStopButton.setImage(image, for: .normal)
        let text = NavigationManager.shared.hasStarted.value ? Utils.localizedString(forKey: "stop_course_button", app: "SARARegate") : Utils.localizedString(forKey: "play_button", app: "SARARegate")
        self.playStopButton.accessibilityLabel = text
    }
     
    // Start timer with interval of 1s
    func startTimer() {
        NavigationManager.shared.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        AnnouncementManager.shared.playFoghorn()
    }
    
    // Stop timer and update timer view
    func stopTimer(isAddingTimerView: Bool = true) {
        NavigationManager.shared.timer?.invalidate()
        NavigationManager.shared.timer = nil
        NavigationManager.shared.totalTime = 0
        if isAddingTimerView {
            addTimerView()
        }
    }
    
    // Decrement timer and update view
    @objc func updateTimer() {
        if NavigationManager.shared.totalTime != 0 {
            NavigationManager.shared.totalTime -= 1  // decrease counter timer
        } else { // End of timer and start
            AnnouncementManager.shared.playFoghorn()
            NavigationManager.shared.addAnnouncementStartLaunch()
            NavigationManager.shared.timer?.invalidate()
            NavigationManager.shared.timer = nil
            self.timerConstraint.isActive = true // remove timer view
            self.timerView.isHidden = true
            // disabled plot button
//            plotButton.isEnabled = false
//            self.plotButton.isEnabled = LocationManager.shared.accuracyVariable.value ?? 100 < 100 // Enable plot button after the timer if GPS accuracy enought
        }
        self.timerView.setTimer(value: NavigationManager.shared.totalTime)
    }
    
    // Add timer only if there is a start for first mark
    func addTimerView() {
        guard let course = NavigationManager.shared.route.value else {
            return
        }
        let isStart = course.isStartFirstMark()
        self.timerConstraint.isActive = !isStart // Add timer view: disable timer constraint
        self.timerView.isHidden = !isStart
        if isStart {
            NavigationManager.shared.initTotalTime()
            self.timerView.plusButton.isEnabled = true // enable button
            self.timerView.minusButton.isEnabled = true // enable button
            self.timerView.setTimer(value: NavigationManager.shared.totalTime)
        }
    }
    
    public func setTimerView(value: Int) {
        self.timerView.setTimer(value: value)
    }
    
    // MARK: - IBActions
    
    @IBAction func pressChooseCourse(_ sender: UIButton) {
        let coursesMenu = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        
        // Adding deselect current route action if there is a route selected
        if NavigationManager.shared.route.value != nil {
            let unselectAction = UIAlertAction(title: Utils.localizedString(forKey: "deactivate_course", app: "SARARegate"), style: .destructive, //destructive
                handler: { _ in
                    NavigationManager.shared.setCurrentRoute(route: nil)
                    NavigationManager.shared.isNotFirstPoint.accept(false)
                    NavigationManager.shared.isNotLastPoint.accept(false)
                    NavigationManager.shared.timer?.invalidate()
                    NavigationManager.shared.timer = nil
                    UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.chooseCourseButton)
            })
            coursesMenu.addAction(unselectAction)
        }

        // Remove course that don't have marks, or marks have no coordinates
        for course in Course.allSortedByProximityFirstPoint().filter({ !$0.points.isEmpty && $0.allPointsHasCoordinates()}) {
            // Create action for each of the route in the app
            let courseAction = UIAlertAction(title: course.name, style: .default, handler: { [weak self] _ in
                NavigationManager.shared.setCurrentRoute(route: course)
                UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self?.chooseCourseButton)
            })
            coursesMenu.addAction(courseAction)
        }
        
        // Adding cancel action to go back
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: self.bundle, comment: ""), style: .cancel, handler: nil)
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        coursesMenu.addAction(cancelAction)
        
        // Present action sheet
        present(coursesMenu, animated: true, completion: nil)
    }
    
    // Go to previous point
    @IBAction func pressPrevious(_ sender: UIButton) {
        NavigationManager.shared.setPreviousPoint()
    }
    // Go to previous next
    @IBAction func pressNext(_ sender: UIButton) {
        // Start route if it's not the case
        NavigationManager.shared.hasStarted.accept(true)
        NavigationManager.shared.setNextPoint()
        playButtonImageAccessibility()
    }
    
    @IBAction func pressPlayStop(_ sender: UIButton) {
        NavigationManager.shared.startStopRoute()
        playButtonImageAccessibility()
        guard let course = NavigationManager.shared.route.value else { return }
        let isStart = course.isStartFirstMark()
        if isStart { // Start or stop timer when pressing button
            if NavigationManager.shared.route.value?.status == .shared && NavigationManager.shared.hasStarted.value { // Shared route, add alert to launch collective start
                let title = Utils.localizedString(forKey: "shared_course", app: "SARARegate")
                let message = NavigationManager.shared.hasStarted.value ? Utils.localizedString(forKey: "collective_start_master", app: "SARARegate") : Utils.localizedString(forKey: "collective_stop_master", app: "SARARegate")
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
                let actionYes = UIAlertAction(title: NSLocalizedString("yes", bundle: self.bundle, comment: ""), style: .default, handler: {_ in
                    FirebaseManager.shared.collectiveStartMaster()
                    NavigationManager.shared.hasStarted.value ? self.startTimer() : self.stopTimer()
                    // disabled if start course
                    self.timerView.plusButton.isEnabled = !NavigationManager.shared.hasStarted.value
                    self.timerView.minusButton.isEnabled = !NavigationManager.shared.hasStarted.value
                })
                let actionNo = UIAlertAction(title: NSLocalizedString("no", bundle: self.bundle, comment: ""), style: .cancel, handler: { _ in
                    NavigationManager.shared.hasStarted.value ? self.startTimer() : self.stopTimer()
                    // disabled if start course
                    self.timerView.plusButton.isEnabled = !NavigationManager.shared.hasStarted.value
                    self.timerView.minusButton.isEnabled = !NavigationManager.shared.hasStarted.value
                })
                alertController.addAction(actionYes)
                alertController.addAction(actionNo)
//                if NavigationManager.shared.hasStarted.value { // For now, just handle collective start
                    self.present(alertController, animated: false)
//                }
            } else {
                NavigationManager.shared.hasStarted.value ? self.startTimer() : self.stopTimer()
                // disabled if start course
                self.timerView.plusButton.isEnabled = !NavigationManager.shared.hasStarted.value
                self.timerView.minusButton.isEnabled = !NavigationManager.shared.hasStarted.value
            }
        }
        // plot button disabled
        plotButton.isHidden = true
//        plotButton.isEnabled = true
//        plotButton.isEnabled = !NavigationManager.shared.hasStarted.value
    }
    
    @IBAction func pressMute(_ sender: UIButton) {
        if AnnouncementQueueManager.shared.muted {
            muteButton.setImage(UIImage(systemName: "speaker.wave.3.fill"), for: .normal)
//            muteButton.setImage(UIImage(named: "volume-up", in: self.bundle, compatibleWith: nil), for: .normal)
            AnnouncementQueueManager.shared.muted = false
            self.muteButton.accessibilityLabel = NSLocalizedString("deactivate_announces", bundle: bundle, comment: "")
            if !UIAccessibility.isVoiceOverRunning {
                AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("activate_announces_message", bundle: bundle, comment: ""), priority: 0, type: .mute, annonceState: nil))
            } else {
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when) {
                    UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("activate_announces_message", bundle: self.bundle, comment: ""))
                }
            }
        } else {
            muteButton.setImage(UIImage(systemName: "speaker.slash.fill"), for: .normal)
//            muteButton.setImage(UIImage(named: "volume-down", in: self.bundle, compatibleWith: nil), for: .normal)
            AnnouncementQueueManager.shared.muted = true
            self.muteButton.accessibilityLabel = NSLocalizedString("activate_announces", bundle: bundle, comment: "")
            if !UIAccessibility.isVoiceOverRunning {
                AnnouncementQueueManager.shared.addAnnounce(Announce(text: NSLocalizedString("deactivate_announces_message", bundle: bundle, comment: ""), priority: 0, type: .mute, annonceState: nil))
            } else {
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when) {
                    UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("deactivate_announces_message", bundle: self.bundle, comment: ""))
                }
            }
        }
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "navigationPage" {
            guard let controller = segue.destination as? NavigationPagingViewController else {
                return
            }
            controller.infoTypes = [.current, .statistic]
            self.navigationPagingController = controller
        }
    }
}
