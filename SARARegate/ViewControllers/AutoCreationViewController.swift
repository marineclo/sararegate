//
//  AutoCreationViewController.swift
//  SARARegate
//
//  Created by Marine on 19.04.21.
//

import UIKit
import SARAFramework
import MapKit
import RxSwift
import RxCocoa

class AutoCreationViewController: UIViewController, CoordinatesViewDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var okButton: UIBarButtonItem!
    @IBOutlet weak var titleItem: UINavigationItem!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var windAxisSliderView: CustomSliderView!
    @IBOutlet weak var startLineLengthSliderView: CustomSliderView!
    @IBOutlet weak var startWindwardLengthSliderView: CustomSliderView!
    @IBOutlet weak var finishLineLengthSliderView: CustomSliderView!
    @IBOutlet weak var leewardFinishLengthSliderView: CustomSliderView!
    @IBOutlet weak var offsetMarkLengthSliderView: CustomSliderView!
    
    @IBOutlet weak var sliderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var offsetMarkConstraint: NSLayoutConstraint!
    @IBOutlet weak var leewardFinishConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var refBuoyLabel: UILabel!
    @IBOutlet weak var refBuoyButton: UIButton!
    @IBOutlet weak var coordinatesLabel: UILabel!
    @IBOutlet weak var coordinatesView: CoordinatesView!
    @IBOutlet weak var generateButton: UIButton!
    
    //MARK: - Variables
    let bundle = Utils.bundle(anyClass: CreateCourseViewController.classForCoder())
    var viewModel: CreateRaceCourseViewModel?
    let disposeBag = DisposeBag()
    var isCourseGenerated: Bool = false
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.titleItem.title = Utils.localizedString(forKey: "auto_creation", app: "SARARegate")
        self.cancelButton.title = NSLocalizedString("cancel", bundle: bundle, comment: "")
        self.okButton.title = NSLocalizedString("ok", bundle: bundle, comment: "")
        
        // Slider
        self.windAxisSliderView.customize(with: SliderInfos(title: Utils.localizedString(forKey: "wind_axis", app: "SARARegate"), unit: .degrees, value: Float(viewModel?.windAxis ?? 180), minValue: 1, maxValue: 360, step: 5, isContinuous: false), accessibilityTrait: .header)
        self.startLineLengthSliderView.customize(with: SliderInfos(title: Utils.localizedString(forKey: "start_line_length", app: "SARARegate"), unit: .meter, value: Float(viewModel?.startLength ?? 200), minValue: 20, maxValue: 800, step: 20, isContinuous: false), accessibilityTrait: .header)
        self.startWindwardLengthSliderView.customize(with: SliderInfos(title: Utils.localizedString(forKey: "start_windward_length", app: "SARARegate"), unit: .meter, value: Float(viewModel?.startWindwardLength ?? 700), minValue: 50, maxValue: 1200, step: 50, isContinuous: false), accessibilityTrait: .header)
        self.finishLineLengthSliderView.customize(with: SliderInfos(title: Utils.localizedString(forKey: "finish_line_length", app: "SARARegate"), unit: .meter, value: Float(viewModel?.finishLength ?? 200), minValue: 20, maxValue: 800, step: 20, isContinuous: false), accessibilityTrait: .header)
        self.leewardFinishLengthSliderView.customize(with: SliderInfos(title: Utils.localizedString(forKey: "leeward_finish_length", app: "SARARegate"), unit: .meter, value: Float(viewModel?.leewardFinishLength ?? 400), minValue: 20, maxValue: 1000, step: 20, isContinuous: false), accessibilityTrait: .header)
        if self.viewModel?.type.value == .bananeDogleg || self.viewModel?.type.value == .bananeDoglegLeewardGate {
            self.offsetMarkConstraint.isActive = true
            self.offsetMarkLengthSliderView.customize(with: SliderInfos(title: Utils.localizedString(forKey: "offset_mark_length", app: "SARARegate"), unit: .meter, value: Float(viewModel?.offsetMarkLength ?? 100), minValue: 10, maxValue: 1000, step: 10, isContinuous: false), accessibilityTrait: .header)
            self.viewHeightConstraint.constant = self.viewHeightConstraint.constant + self.sliderViewHeightConstraint.constant
        } else {
            self.offsetMarkConstraint.isActive = false
            self.offsetMarkLengthSliderView.isHidden = true
            if self.viewModel?.type.value == .matchRace {
                // In match race, no need to distance between leeeward and finish line
                self.leewardFinishConstraint.isActive = true
                self.leewardFinishLengthSliderView.isHidden = true
                self.viewHeightConstraint.constant = self.viewHeightConstraint.constant - self.sliderViewHeightConstraint.constant
                // The starting line and the finish line are the same, disable finish line 
                self.startLineLengthSliderView.settingsValue.asObservable()
                    .bind(to: self.finishLineLengthSliderView.settingsValue)
                    .disposed(by: disposeBag)
                self.finishLineLengthSliderView.isEnabled = false
                
            } else {
                self.leewardFinishConstraint.isActive = false
            }
        }
        
        // Mark selection
        self.refBuoyLabel.text = Utils.localizedString(forKey: "ref_buoy", app: "SARARegate")
        self.refBuoyLabel.accessibilityTraits = .header
        self.refBuoyButton.customizePrimary()
        if let name = self.viewModel?.buoyRef.value?.name {
            self.refBuoyButton.setTitle(name, for: .normal)
            self.refBuoyButton.accessibilityLabel = "\(Utils.localizedString(forKey: "ref_buoy", app: "SARARegate")) \(name)"
        } else {
            self.refBuoyButton.setTitle(Utils.localizedString(forKey: "choose_ref_buoy", app: "SARARegate"), for: .normal)
            self.refBuoyButton.accessibilityLabel = Utils.localizedString(forKey: "choose_ref_buoy", app: "SARARegate")
        }
        
        // Coordinates
        self.coordinatesLabel.text = NSLocalizedString("coordinates", bundle: bundle, comment: "")
        self.coordinatesLabel.accessibilityTraits = .header
        // Add new button for connected buoy
        let connectedBuoyButton = UIButton()
        connectedBuoyButton.setImage(UIImage(named: "connected_buoy", in: bundle, compatibleWith: nil), for: .normal)
        connectedBuoyButton.accessibilityLabel = Utils.localizedString(forKey: "use_connected_buoy", app: "SARARegate")
        connectedBuoyButton.addTarget(self, action: #selector(connectedBuoyAction), for: .touchUpInside)
        connectedBuoyButton.tintColor = Utils.tintIcon
        connectedBuoyButton.isEnabled = false
        self.coordinatesView.delegate = self
        self.coordinatesView.setButtonOtherMethod(button: connectedBuoyButton)
        if let gpsPosRefBuoy = self.viewModel?.buoyRef.value?.gpsPosition {
            self.coordinatesView.fillCoordinates(with: gpsPosRefBuoy, auto: true)
        }
        
        Observable.combineLatest(self.viewModel!.buoyRef.asObservable(), self.coordinatesView.observableCoordinates()) { (buoyRef, filledCoordinates) in
            return buoyRef != nil && filledCoordinates
        }
        .bind(to:  self.generateButton.rx.isEnabled)
        .disposed(by: disposeBag)
        
        // Generate
        self.generateButton.customizePrimary()
        self.generateButton.setTitle(Utils.localizedString(forKey: "generate_course", app: "SARARegate"), for: .normal)
        self.generateButton.accessibilityLabel = Utils.localizedString(forKey: "generate_course", app: "SARARegate")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name(rawValue:"modalSelectOnMapIsDimissed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(note:)), name: NSNotification.Name("modalChooseBuoyIsDimissed"), object: nil)
        //  Scroll keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleNotification(note: Notification) {
        switch note.name {
        case NSNotification.Name(rawValue:"modalSelectOnMapIsDimissed"):
            let annotation = note.userInfo!["annotation"] as! MKAnnotation
            let gpsPosition = GPSPosition(latitude: Float(annotation.coordinate.latitude), longitude: Float(annotation.coordinate.longitude))
            self.coordinatesView.fillCoordinates(with: gpsPosition, auto: true)
        case NSNotification.Name("modalChooseBuoyIsDimissed"):
            let buoy = note.userInfo!["buoy"] as! Buoy
            self.refBuoyButton.setTitle(buoy.name, for: .normal)
            self.refBuoyButton.accessibilityLabel = "\(Utils.localizedString(forKey: "ref_buoy", app: "SARARegate")) \(buoy.name)"
            self.viewModel?.buoyRef.accept(buoy)
            // Fill coordinates
            if let gpsPos = buoy.gpsPosition { // The buoy has already some coordinates
                self.coordinatesView.fillCoordinates(with: gpsPos, auto: true)
            }
            UIAccessibility.post(notification: .layoutChanged, argument: self.refBuoyButton)
        default:
            return
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            // if keyboard size is not available for some reason, dont do anything
            return
        }
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        // reset back the content inset to zero after keyboard is gone
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func updateAutoParamsModel() {
        self.viewModel?.windAxis = Int(windAxisSliderView.settingsValue.value)
        self.viewModel?.startWindwardLength = Int(startWindwardLengthSliderView.settingsValue.value)
        self.viewModel?.startLength = Int(startLineLengthSliderView.settingsValue.value)
        self.viewModel?.finishLength = Int(finishLineLengthSliderView.settingsValue.value)
        self.viewModel?.leewardFinishLength = Int(leewardFinishLengthSliderView.settingsValue.value)
        self.viewModel?.offsetMarkLength = Int(offsetMarkLengthSliderView.settingsValue.value)
    }
    
    func generateCourse() {
        guard let _refBuoy = self.viewModel?.buoyRef.value, let (latitude, longitude) = self.coordinatesView.getCoordinates() else {
            return
        }
        let gpsPos = GPSPosition(latitude: latitude, longitude: longitude)
        let refBuoy = Buoy(id: nil, name: _refBuoy.name, gpsPosition: gpsPos, toLetAt: Buoy.ToLetAt(rawValue: _refBuoy.toLetAt())!, type: _refBuoy.type.value)
        self.viewModel?.constructCourseBased(on: refBuoy)
        let alertController = UIAlertController(title: "", message: Utils.localizedString(forKey: "generated_course", app: "SARARegate"), preferredStyle: .alert)
        let action = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .default, handler: { _ in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalAutoCreationIsDimissed"), object: nil)
            self.dismiss(animated: false, completion: nil)
        })
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - IBActions
    @IBAction func cancelAction(_ sender: Any) {
        // close view
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func okAction(_ sender: Any) {
        updateAutoParamsModel()
        // if the user doesn't generate the course, generate it otherwise display what's missing to generate it.
        if !self.isCourseGenerated && self.generateButton.isEnabled {
            generateCourse()
        } else if self.isCourseGenerated && self.generateButton.isEnabled {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalAutoCreationIsDimissed"), object: nil)
            self.dismiss(animated: false, completion: nil)
        } else {
            // Check what's missing to generate the course
            var errorMessage: String = Utils.localizedString(forKey: "not_generated_course", app: "SARARegate")
            if self.viewModel!.buoyRef.value == nil && self.coordinatesView.getCoordinates() == nil {
                errorMessage += Utils.localizedString(forKey: "ref_buoy_and_coord_missing", app: "SARARegate")
            }
            if  self.viewModel!.buoyRef.value != nil && self.coordinatesView.getCoordinates() == nil {
                errorMessage += Utils.localizedString(forKey: "ref_buoy_coord_missing", app: "SARARegate")
            }
            
            let alertController = UIAlertController(title: NSLocalizedString("warning", bundle: bundle, comment: ""), message: errorMessage, preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("ok", bundle: bundle, comment: ""), style: .default, handler: { _ in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalAutoCreationIsDimissed"), object: nil)
                self.dismiss(animated: false, completion: nil)
            })
            let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", bundle: bundle, comment: ""), style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func generateAction(_ sender: Any) {
        self.isCourseGenerated = true
        updateAutoParamsModel()
        // Generate course
        generateCourse()
    }
    
    @objc func connectedBuoyAction(sender: UIButton!) {
        print("Button tapped")
    }
    
    // MARK: - CoordinatesViewDelegate
    func openSelectOnMap(tagView: Int?) {
        let storyboard = UIStoryboard(name: "Route", bundle: bundle)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "SelectOnMapVC") as? SelectPositionOnMapViewController else {
                return
        }
        if let (latitude, longitude) = self.coordinatesView.getCoordinates() {
            let annotation = MapAnnotation(latitude: Double(latitude), longitude: Double(longitude), type: .mark, isCurrent: true)
                controller.annotation = annotation
        } else {
            let annotation = MapAnnotation(latitude: Double.nan, longitude: Double.nan, type: .mark, isCurrent: true)
            controller.annotation = annotation
        }
        self.present(controller, animated: true, completion: nil)
    }
    
    func useCurrentPosition(tagView: Int?, gpsPosition: GPSPosition) {
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when) {
            UIAccessibility.post(notification: UIAccessibility.Notification.announcement, argument: NSLocalizedString("coordinates_used_for_this_point", bundle: self.bundle, comment: ""))
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "chooseBuoySegue" {
            guard let destination = segue.destination as? ChooseBuoyViewController else {
                return
            }
            var buoys: [Buoy] = []
            // Match Race
            if self.viewModel?.type.value == .matchRace {
                let start = self.viewModel?.getTempPoints().first as! Mark
                let red_mark = self.viewModel?.getTempPoints()[1] as! Mark
                buoys.append(start.buoys[0])
                buoys.append(start.buoys[1])
                buoys.append(red_mark.buoys[0])
            } else {
                for mark in self.viewModel!.getTempPoints() {
                    (mark as! Mark).buoys.forEach({
                        if !buoys.contains($0) { // get a list of unique buoy
                            buoys.append($0)
                        }
                    })
                }
            }
            destination.buoys = buoys
        }
    }
}
