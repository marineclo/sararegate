//
//  TabBarController.swift
//  SARARegate
//
//  Created by Marine on 26.03.21.
//

import UIKit
import SARAFramework
import RxSwift
import FirebaseAuth

class TabBarController: UITabBarController, AnnouncementDelegate, LocalizableString, UITabBarControllerDelegate, FirebaseManagerDelegate {
    
    
    // MARK: Variables
    let disposeBag = DisposeBag()
    let bundle = Utils.bundle(anyClass: TabBarController.classForCoder())
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self // UITabBarControllerDelegate
        NavigationManager.shared.announcementDelegate = self
        FirebaseManager.shared.delegate = self
        AnnouncementManager.shared.announcementDelegate = self
        Utils.localizableProtocol = self
        FirebaseManager.shared.retrieveAllCourses()
        
        NavigationManager.shared.route
            .asObservable()
            .subscribe(onNext: { [weak self] (_) in
                if NavigationManager.shared.updateNavView { // When a route is activated
                    self?.selectedIndex = 0 // Go to the navigation tabs
                }
            }).disposed(by: disposeBag)
                
        FirebaseManager.shared.removedRouteName
            .asObservable()
            .subscribe(onNext: { routeName in
                let title = NSLocalizedString("course_deleted", bundle: self.bundle, comment: "")
                let message = String(format: NSLocalizedString("subscribed_course_deleted", bundle: self.bundle, comment: ""), routeName)
                if self.selectedIndex == 1 { // Route tab
                    if let topController = UIApplication.topViewController() {
                        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
                        let action = UIAlertAction(title: NSLocalizedString("ok", bundle: self.bundle, comment: ""), style: .default, handler: {_ in
                            self.dismiss(animated: false, completion: { // Dismiss all viewControllers concerning the route that is removed
                                while UIApplication.topViewController() != self.viewControllers?[1] {
                                    self.dismiss(animated: false, completion: {
                                          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCourseIsDimissed"), object: nil)
                                    })
                                }
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalCreateCourseIsDimissed"), object: nil)
                            })
                        })
                        alertController.addAction(action)
                        topController.present(alertController, animated: false)
                    }
                } else { // Other tabs
                    if let topController = UIApplication.topViewController() {
                        let alert = Utils.showAlert(with: title, message: message)
                        topController.present(alert, animated: false)
                    }
                }
            }).disposed(by: disposeBag)
        
        FirebaseManager.shared.collectiveStart
            .asObservable()
            .subscribe(onNext: { dict in
                if let timer = dict["timer"] as? Int, let timestamp = dict["timestamp"] as? Int, let courseId = dict["id"] as? String {
                    if self.selectedIndex == 0 && NavigationManager.shared.route.value?.id == courseId {
                        // Launch start without information: the user in on the nav tab and the course is already activated
                        let currentTimestamp = Int(Date().timeIntervalSince1970)
                        let t = currentTimestamp - timestamp
                        if t >= 0 {
                            NavigationManager.shared.setCurrentRoute(route: nil)
                            NavigationManager.shared.setCurrentRoute(route: Course.getRealmCourse(courseId: courseId))
                            NavigationManager.shared.totalTime = timer - Int(t)
                            (self.viewControllers?[0] as? NavigationViewController)?.setTimerView(value: NavigationManager.shared.totalTime)
                            (self.viewControllers?[0] as? NavigationViewController)?.playStopButton.sendActions(for: .touchUpInside)
                        }
                    } else {
                        // Add alert and if the user answers yes, go to navigation tab and launch start
                        if let topController = UIApplication.topViewController() {
                            let title =  Utils.localizedString(forKey: "shared_course", app: "SARARegate")
                            let message = Utils.localizedString(forKey: "collective_start_subscribers", app: "SARARegate")
                            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                            let actionYes = UIAlertAction(title: NSLocalizedString("yes", bundle: self.bundle, comment: ""), style: .default, handler: {_ in
                                let currentTimestamp = Int(Date().timeIntervalSince1970)
                                let t = currentTimestamp - timestamp
                                var totalTime: Int = 0
                                if t >= 0 && t < timer {
                                    totalTime = timer - Int(t) // Update total time
                                } else { // Start done
                                    totalTime = 0
                                }
                                NavigationManager.shared.updateNavView = true // Redirect to the nav tab
                                NavigationManager.shared.setCurrentRoute(route: Course.getRealmCourse(courseId: courseId)) // Set new Route
                                NavigationManager.shared.totalTime = totalTime // Update total time
                                (self.viewControllers?[0] as? NavigationViewController)?.setTimerView(value: NavigationManager.shared.totalTime) // Update timer view with new value
                                (self.viewControllers?[0] as? NavigationViewController)?.playStopButton.sendActions(for: .touchUpInside) // Launch start
                            })
                            let actionNo = UIAlertAction(title: NSLocalizedString("no", bundle: self.bundle, comment: ""), style: .cancel, handler: nil)
                            alert.addAction(actionYes)
                            alert.addAction(actionNo)
                            topController.present(alert, animated: false)
                        }
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        // Set the tab bar title at runtime
        guard let items = self.tabBar.items else {
            return
        }
        let bundle = Utils.bundle(anyClass: self.classForCoder)

        items[0].image = UIImage(named: "sailboatTabBar", in: bundle, compatibleWith: nil)
        items[1].image = UIImage(named: "itineraryTabBar", in: bundle, compatibleWith: nil)
        items[2].image = UIImage(named: "bell", in: bundle, compatibleWith: nil)
        items[3].image = UIImage(named: "mapTabBar", in: bundle, compatibleWith: nil)
        items[4].image = UIImage(named: "settingsTabBar", in: bundle, compatibleWith: nil)
        
        items[0].title = NSLocalizedString("navigation", bundle: bundle, comment: "")
        items[1].title = Utils.localizedString(forKey: "menu_courses", app: "SARARegate")
        items[2].title = NSLocalizedString("menu_announces", bundle: bundle, comment: "")
        items[3].title = NSLocalizedString("menu_plotter", bundle: bundle, comment: "")
        items[4].title = NSLocalizedString("menu_settings", bundle: bundle, comment: "")
        
        if let vc = self.viewControllers?[1] as? CoursesMenuViewController {
            vc.setNavigationTitle(title: Utils.localizedString(forKey: "menu_courses", app: "SARARegate"))
            let tabsMenuItems = [
                Utils.localizedString(forKey: "menu_my_courses", app: "SARARegate"),
                Utils.localizedString(forKey: "menu_subscription", app: "SARARegate")
            ]
            vc.setTabMenuItems(items: tabsMenuItems)
            
            var pagesViewControllers: [UIViewController] = []
            // My Courses
            if let coursesViewController = UIStoryboard(name: "Main", bundle: Bundle(for: TabBarController.classForCoder())).instantiateViewController(withIdentifier: "MyCoursesViewController") as? CoursesViewController {
                pagesViewControllers.append(coursesViewController)
            }
            // Subscription
            if let subscriptionViewController = UIStoryboard(name: "Route", bundle: Bundle(for: TabBarController.classForCoder())).instantiateViewController(withIdentifier: "SubscriptionViewController") as? SubscriptionViewController {
                pagesViewControllers.append(subscriptionViewController)
            }
//            if let sharingViewController = UIStoryboard(name: "Utils", bundle: Bundle(for: TabBarController.classForCoder())).instantiateViewController(withIdentifier: "SelectTableViewController") as? SelectTableViewController {
//                sharingViewController.titleTableHeader = Utils.localizedString(forKey: "subscriptions_available", app: "SARARegate")
//                // TODO: retrieve courses
////                sharingViewController.objects = FirebaseManager.shared.coursesName as [AnyObject]
//                sharingViewController.displayNavigationBar = false
//                pagesViewControllers.append(sharingViewController)
//            }
            vc.setPagesViewController(viewControllers: pagesViewControllers)
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let vc = viewController as? TracerViewController {
            vc.isSARARace = true
        } else if let _ = viewController as? CoursesMenuViewController {
//            print(FirebaseManager.shared.coursesName)
//            vc.objects = FirebaseManager.shared.coursesName as [AnyObject]
        }
        return true
    }
    
    // MARK: - AnnouncementDelegate
    
    // Announcement for crossing point that is not the last one of the course
    func announcementPointValidation(nextPoint: AnyPoint?, noCoordMessage: String?) -> String {
        guard let mark = NavigationManager.shared.point.value?.point as? Mark, let nxtPoint = nextPoint else {
            return ""
        }
        let markType = mark.getMarkType()
        switch markType {
        case Mark.MarkType.simple.rawValue: // Around buoy axis XX meters. Mark "name next buoy" activated
            // Calculate distance between current position and the buoy
            let distance = Utils.tupleDistance(floatDistance: NavigationHelper.getDistanceFrom(location: LocationManager.shared.getCurrentPosition(), to: mark.buoys.first?.gpsPosition))
            let distanceAnnoucement = AnnouncementQueueManager.shared.annonceDistanceStart(tupleDistance: distance)
            return "\(Utils.localizedString(forKey: "axis_buoy", app: "SARARegate")) \(distanceAnnoucement). \(String(format: Utils.localizedString(forKey: "mark_activated", app: "SARARegate"), nxtPoint.point.name))"
        case Mark.MarkType.start.rawValue: // start line crossed. Mark "name next mark" activated
            return "\(Utils.localizedString(forKey: "start_crossed", app: "SARARegate")). \(String(format: Utils.localizedString(forKey: "mark_activated", app: "SARARegate"), nxtPoint.point.name))"
        case Mark.MarkType.end.rawValue: // Finish line crossed
            return "\(Utils.localizedString(forKey: "finish_crossed", app: "SARARegate"))."
        case Mark.MarkType.double.rawValue:
            return "\(String(format: Utils.localizedString(forKey: "mark_activated", app: "SARARegate"), nxtPoint.point.name))"
        default:
            return ""
        }
    }
    
    // Announcement for last point in the course
    func announcementEndRoute(noCoordMessage: String?, noCoord: Bool) -> String {
        guard let mark = NavigationManager.shared.point.value?.point as? Mark else {
            return ""
        }
        let markType = mark.getMarkType()
        switch markType {
        case Mark.MarkType.simple.rawValue, Mark.MarkType.double.rawValue: // Last mark crossed
            return "\(Utils.localizedString(forKey: "last_mark_crossed", app: "SARARegate"))."
        case Mark.MarkType.start.rawValue: // start line crossed.
            return "\(Utils.localizedString(forKey: "start_crossed", app: "SARARegate"))."
        case Mark.MarkType.end.rawValue: // Finish line crossed
            return "\(Utils.localizedString(forKey: "finish_crossed", app: "SARARegate"))."
        default:
            return ""
        }
    }
    
    func announcementAxis() -> String {
        return Utils.localizedString(forKey: "route_axis", app: "SARARegate")
    }
    
    func announcementStartLaunch() -> String {
        return Utils.localizedString(forKey: "start_launched", app: "SARARegate")
    }
    
    func profileRouteTitle() -> String {
        return Utils.localizedString(forKey: "course_title", app: "SARARegate")
    }
    
    func deleteMarkUsedRouteMessage() -> String {
        return Utils.localizedString(forKey: "warning_delete_mark_course", app: "SARARegate")
    }
    
    // MARK: - LocalizableString
    func aboutText() -> String {
        return Utils.localizedString(forKey: "about", app: "SARARegate")
    }
    
    // MARK: - FirebaseManagerDelegate
    func courseChanged(course: SARAFramework.Course?) {}
    
    func courseRemoved(courseId: String) {}
    
    func courseAdded(course: SARAFramework.Course?) {}
}
