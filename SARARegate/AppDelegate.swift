//
//  AppDelegate.swift
//  SARARegate
//
//  Created by Marine on 26.03.21.
//

import UIKit
import RealmSwift
import Firebase
import SARAFramework
import AVFoundation

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        var directory: String
        var nameDatabase: String
        if BuildConfiguration.shared.environment == .debugDevelopment {
            print("DEBUG")
            directory = "Debug"
            nameDatabase = "debug"
        } else if BuildConfiguration.shared.environment == .releaseProd {
            print("RELEASE")
            directory = "Prod"
            nameDatabase = "prod"
        } else {
            print("TESTFLIGHT")
            directory = "TestFlight"
            nameDatabase = "testFlight"
        }

        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let docURL = URL(string: documentsDirectory)!
        let dataPath = docURL.appendingPathComponent(directory)
        if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription);
            }
        }
        let config = Realm.Configuration(
            fileURL: dataPath.appendingPathComponent("\(nameDatabase).realm"),
            schemaVersion: 6,
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 4 {
                    var profileId: String?
                    migration.enumerateObjects(ofType: "ProfileVoiceRealm") { oldObject, newObject in
                        guard let oldObject = oldObject else { return }
                        guard let id = oldObject["id"] as? String else { return }
                        guard let vocalRate = oldObject["vocalRate"] as? Float else { return }
                        guard let voice = oldObject["voice"] as? String else { return }
                        guard let batteryLevel = oldObject["batteryLevel"] as? Object else { return }
                        
                        let rProfileSetting: MigrationObject = migration.create("ProfileSetting")
                        profileId = batteryLevel["id"] as? String
                        rProfileSetting["id"] = batteryLevel["id"]
                        rProfileSetting["isActivated"] = batteryLevel["isActivated"]
                        rProfileSetting["threshold"] = batteryLevel["threshold"]
                        rProfileSetting["timeThreshold"] = batteryLevel["timeThreshold"]
                        
                        let rVarious: MigrationObject = migration.create("ProfileVariousRealm")
                        rVarious["id"] = id
                        rVarious["vocalRate"] = vocalRate
                        rVarious["voice"] = voice
                        rVarious["batteryLevel"] = rProfileSetting
                        
                        if newObject != nil {
                            migration.delete(newObject!)
                        }
                        
                    }
                    var isDeleted: Bool = false
                    migration.enumerateObjects(ofType: "ProfileSetting", { oldObject, newObject in
                        guard let id = oldObject?["id"] as? String else { return }
                        if profileId != nil && id == profileId! && newObject != nil && !isDeleted {
                            migration.delete(newObject!)
                            isDeleted = true
                        }
                            
                    })
                    migration.enumerateObjects(ofType: "AnyProfileTypeRealm", { oldObject, newObject in
                        guard let oldObject = oldObject else { return }
                        guard let typeName = oldObject["typeName"] as? String else { return }
                        if typeName == "ProfileVoiceRealm" {
                            newObject?["typeName"] = "ProfileVariousRealm"
                        }
                    })
                }
                if oldSchemaVersion < 5 {
                    migration.enumerateObjects(ofType: Settings.className(), { _, newObject in
                        newObject!["saveTrace"] = false
                    })
                }
            }
        )
        Realm.Configuration.defaultConfiguration = config
        
        FirebaseApp.configure()
        if let userID = Auth.auth().currentUser?.uid { // Register for push notification
            let pushManager = PushNotificationManager(userID: userID)
            pushManager.registerForPushNotifications()
        }
        //
        FirebaseManager.shared.checkSubscribedCourse()
        
        // Set Default profile
        var profileTypes: [ProfileType] = []
        let defaultValues = ProfileSetting.defaultProfileSettingValues
        let defaultValuesActivated = ProfileSetting.defaultProfileActivatedSettingValues
        let defaultSpeedValues = ProfileSetting.defaultSpeedProfileSettingValues
        let defaultSpeedValuesActivated = ProfileSetting.defaultSpeedProfileActivatedSettingValues
        let profileGPS = ProfileGPS(compass: ProfileSetting(defaultValues: defaultValues), courseOverGround: ProfileSetting(defaultValues: defaultValuesActivated), speedOverGround: ProfileSetting(defaultValues: defaultSpeedValuesActivated), maxSpeedOverGround: ProfileSetting(defaultValues: defaultSpeedValues))
        profileTypes.append(profileGPS)
        let profileRoute = ProfileRoute(azimut: ProfileSetting(defaultValues: defaultValues), gisement: ProfileSetting(defaultValues: defaultValuesActivated), gisementTime: ProfileSetting(isActivated: false, threshold: 1, timeThreshold: 10), distance: ProfileSetting(isActivated: true, threshold: 20, timeThreshold: 10), cmg: ProfileSetting(defaultValues: defaultSpeedValues), distanceToSegment: nil, entry3Lengths: ProfileSetting(defaultValues: defaultValues))
        profileTypes.append(profileRoute)
        let profileVarious = ProfileVarious(voice: "WomanVoice", vocalRate: 50, batteryLevel: ProfileSetting(defaultValues: defaultValuesActivated))
        profileTypes.append(profileVarious)
        
        Settings.shared.defaultProfileTypes = profileTypes
        
        Settings.shared.load(distanceUnit: NavigationUnitRealm(rawUnit: NavigationUnit.meter.rawValue), traceSensitivity: 5) // By default the unit for the distance is meter and the sensitivity for the trace is 5m.
        LocationManager.shared.load()
        
        // Change automatic calculation of the diatance for annoncement to a percentage of the distance
        ProfileRouteRealm.updateDistancePercent()
        
        // Set Color
        Utils.secondaryColor = #colorLiteral(red: 1, green: 0.3333333333, blue: 0, alpha: 1)
        
        // White non-transucent navigatio bar, supports dark appearance
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
            let appearanceTabBar = UITabBarAppearance()
            appearanceTabBar.configureWithOpaqueBackground()
            UITabBar.appearance().standardAppearance = appearanceTabBar
            UITabBar.appearance().scrollEdgeAppearance = appearanceTabBar
        }
        
        UITabBar.appearance().tintColor = Utils.tintIcon
        application.isIdleTimerDisabled = true // Avoid app to enter sleep mode
        
//        FirebaseManager.shared.retrieveData()
        
        // Audio playback in background
        if #available(iOS 10.0, *) {
            try! AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.spokenAudio, options: [AVAudioSession.CategoryOptions.interruptSpokenAudioAndMixWithOthers])
        }
        else {
            try? AVAudioSession.sharedInstance().setMode(AVAudioSession.Mode.spokenAudio)
        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

